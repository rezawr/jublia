"""Default configuration

Use env var to override
"""
import os


"""
| ----------------------------------------------------------------
| Some bug on flask cookie cutter that celery wont work because
| the configuration is failed to load. so the global variable
| will be hardcoded right here
| ----------------------------------------------------------------
"""

ENV = "development"
DEBUG = ENV == "development"
SECRET_KEY = "mysecretkey"

SQLALCHEMY_DATABASE_URI = "postgresql://myuser:myuser@127.0.0.1:5432/jublia"
SQLALCHEMY_TRACK_MODIFICATIONS = False
CELERY = {
    "broker_url": "redis://localhost:6379",
    "result_backend": "redis://localhost:6379",
}

MAIL_SERVER = "sandbox.smtp.mailtrap.io"
MAIL_PORT = 465
MAIL_USERNAME = "f53d4dff62cb73"
MAIL_PASSWORD = "30e65dc2166df9"
MAIL_USE_TLS = False
MAIL_DEFAULT_SENDER = "admin@jublia.com"
