import enum

from jublia.extensions import db
from jublia.models import User


class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return "<Event %s>" % self.name
    

class EventReminder(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))
    event = db.relationship("Event", backref=db.backref("event", uselist=False))

    email_subject = db.Column(db.String(50), nullable=False)
    email_content = db.Column(db.String(255), nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return f"<EventReminder {self.event.name} {self.email_subject}>"


class MailStatusChoices(enum.Enum):
    QUEUE = 'Queue'
    PROCESS = "Process"
    FAIL = "Fail"
    SUCESS = "Succes"


class EventReminderMailQueue(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    event_reminder_id = db.Column(db.Integer, db.ForeignKey('event_reminder.id'))
    event_reminder = db.relationship("EventReminder", backref=db.backref("EventReminder", uselist=False))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref=db.backref("User", uselist=False))

    status = db.Column(db.Enum(MailStatusChoices))

    def __repr__(self):
        return f"<EventReminderMailQueue {self.event_reminder.event.name}> {self.user.email} {self.status}"
