from flask import Blueprint, current_app, jsonify
from flask_restful import Api
from marshmallow import ValidationError
from jublia.extensions import apispec
from jublia.api.resources import (
    UserResource, UserList, UserSubscriptionResource, UserSubscriptionList,
    EventResource, EventList, EventReminderList
)
from jublia.api.schemas import UserSchema, UserSubscriptionSchema, EventSchema, EventReminderSchema


blueprint = Blueprint("api", __name__, url_prefix="/api/v1")
api = Api(blueprint)


api.add_resource(UserResource, "/users/<int:user_id>", endpoint="user_by_id")
api.add_resource(UserList, "/users", endpoint="users")

api.add_resource(UserSubscriptionResource, "/users/subscription/<int:user_subscription_id>", endpoint="user_subscription_by_id")
api.add_resource(UserSubscriptionList, "/users/subscription", endpoint="user_subscriptions")

api.add_resource(EventResource, "/events/<int:event_id>", endpoint="event_by_id")
api.add_resource(EventList, "/events", endpoint="events")

api.add_resource(EventReminderList, "/event/reminders", endpoint="event/reminders")


@blueprint.before_app_first_request
def register_views():
    apispec.spec.components.schema("UserSchema", schema=UserSchema)
    apispec.spec.components.schema("UserSubscriptionSchema", schema=UserSubscriptionSchema)
    apispec.spec.components.schema("EventSchema", schema=EventSchema)
    apispec.spec.components.schema("EventReminderSchema", schema=EventReminderSchema)

    apispec.spec.path(view=UserResource, app=current_app)
    apispec.spec.path(view=UserList, app=current_app)
    apispec.spec.path(view=UserSubscriptionResource, app=current_app)
    apispec.spec.path(view=UserSubscriptionList, app=current_app)

    apispec.spec.path(view=EventResource, app=current_app)
    apispec.spec.path(view=EventList, app=current_app)

    apispec.spec.path(view=EventReminderList, app=current_app)


@blueprint.errorhandler(ValidationError)
def handle_marshmallow_error(e):
    """Return json error for marshmallow validation errors.

    This will avoid having to try/catch ValidationErrors in all endpoints, returning
    correct JSON response with associated HTTP 400 Status (https://tools.ietf.org/html/rfc7231#section-6.5.1)
    """
    return jsonify(e.messages), 400
