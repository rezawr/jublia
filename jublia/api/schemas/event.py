from dateutil.parser import parse
from marshmallow import fields

from jublia.models import Event, EventReminder
from jublia.extensions import ma, db



"""
Validate and change format of data from 
“15 Dec 2015 23:12” to python timestamp
"""
def validate_timestamp(value):
    return parse(value)


class EventSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Event
        sqla_session = db.session
        load_instance = True


class EventReminderSchema(ma.SQLAlchemyAutoSchema):
    timestamp = fields.String(validate=validate_timestamp, required=True)

    class Meta:
        model = EventReminder
        sqla_session = db.session
        load_instance = True
        include_fk = True
