from jublia.models import User, UserSubsciprtion
from jublia.extensions import ma, db


class UserSchema(ma.SQLAlchemyAutoSchema):

    id = ma.Int(dump_only=True)
    password = ma.String(load_only=True, required=True)

    class Meta:
        model = User
        sqla_session = db.session
        load_instance = True
        exclude = ("_password",)


class UserSubscriptionSchema(ma.SQLAlchemyAutoSchema):
    id = ma.Int(dump_only=True)

    class Meta:
        model = UserSubsciprtion
        include_relationships = True
        include_fk = True
        sqla_session = db.session
        load_instance = True
