from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from jublia.api.schemas import EventSchema, EventReminderSchema
from jublia.models import Event, EventReminder
from jublia.extensions import db
from jublia.commons.pagination import paginate

class EventResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      summary: Get a event
      description: Get a single event by ID
      parameters:
        - in: path
          name: event_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  event: EventSchema
        404:
          description: event does not exists
    put:
      tags:
        - api
      summary: Update a event
      description: Update a single event by ID
      parameters:
        - in: path
          name: event_id
          schema:
            type: integer
      requestBody:
        content:
          application/json:
            schema:
              EventSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: event updated
                  event: EventSchema
        404:
          description: event does not exists
    delete:
      tags:
        - api
      summary: Delete a event
      description: Delete a single event by ID
      parameters:
        - in: path
          name: event_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: event deleted
        404:
          description: event does not exists
    """

    method_decorators = [jwt_required()]

    def get(self, event_id):
        schema = EventSchema()
        event = Event.query.get_or_404(event_id)
        return {"event": schema.dump(event)}

    def put(self, event_id):
        schema = EventSchema(partial=True)
        event = Event.query.get_or_404(event_id)
        event = schema.load(request.json, instance=event)

        db.session.commit()

        return {"msg": "event updated", "event": schema.dump(event)}

    def delete(self, event_id):
        event = Event.query.get_or_404(event_id)
        db.session.delete(event)
        db.session.commit()

        return {"msg": "event deleted"}


class EventList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      summary: Get a list of events
      description: Get a list of paginated events
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/EventSchema'
    post:
      tags:
        - api
      summary: Create a event
      description: Create a new event
      requestBody:
        content:
          application/json:
            schema:
              EventSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: event created
                  event: EventSchema
    """

    method_decorators = [jwt_required()]

    def get(self):
        schema = EventSchema(many=True)
        query = Event.query
        return paginate(query, schema)

    def post(self):
        schema = EventSchema()
        event = schema.load(request.json)

        db.session.add(event)
        db.session.commit()

        return {"msg": "event created", "event": schema.dump(event)}, 201


class EventReminderList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      summary: Get a list of event reminders
      description: Get a list of paginated event reminders
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/EventReminderSchema'
    post:
      tags:
        - api
      summary: Create a reminder
      description: Create a new reminder
      requestBody:
        content:
          application/json:
            schema:
              EventReminderSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Event reminder created
                  event_reminder: EventReminderSchema
    """

    method_decorators = [jwt_required()]

    def get(self):
        schema = EventReminderSchema(many=True)
        query = EventReminder.query
        return paginate(query, schema)

    def post(self):
        schema = EventReminderSchema()
        reminder = schema.load(request.json)

        db.session.add(reminder)
        db.session.commit()

        return {"msg": "Event reminder created", "event_reminder": schema.dump(reminder)}, 201
