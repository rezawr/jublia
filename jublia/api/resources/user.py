from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from jublia.api.schemas import UserSchema, UserSubscriptionSchema
from jublia.models import User, UserSubsciprtion
from jublia.extensions import db
from jublia.commons.pagination import paginate


class UserResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      summary: Get a user
      description: Get a single user by ID
      parameters:
        - in: path
          name: user_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  user: UserSchema
        404:
          description: user does not exists
    put:
      tags:
        - api
      summary: Update a user
      description: Update a single user by ID
      parameters:
        - in: path
          name: user_id
          schema:
            type: integer
      requestBody:
        content:
          application/json:
            schema:
              UserSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: user updated
                  user: UserSchema
        404:
          description: user does not exists
    delete:
      tags:
        - api
      summary: Delete a user
      description: Delete a single user by ID
      parameters:
        - in: path
          name: user_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: user deleted
        404:
          description: user does not exists
    """

    method_decorators = [jwt_required()]

    def get(self, user_id):
        schema = UserSchema()
        user = User.query.get_or_404(user_id)
        return {"user": schema.dump(user)}

    def put(self, user_id):
        schema = UserSchema(partial=True)
        user = User.query.get_or_404(user_id)
        user = schema.load(request.json, instance=user)

        db.session.commit()

        return {"msg": "user updated", "user": schema.dump(user)}

    def delete(self, user_id):
        user = User.query.get_or_404(user_id)
        db.session.delete(user)
        db.session.commit()

        return {"msg": "user deleted"}


class UserList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      summary: Get a list of users
      description: Get a list of paginated users
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/UserSchema'
    post:
      tags:
        - api
      summary: Create a user
      description: Create a new user
      requestBody:
        content:
          application/json:
            schema:
              UserSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: user created
                  user: UserSchema
    """

    method_decorators = [jwt_required()]

    def get(self):
        schema = UserSchema(many=True)
        query = User.query
        return paginate(query, schema)

    def post(self):
        schema = UserSchema()
        user = schema.load(request.json)

        db.session.add(user)
        db.session.commit()

        return {"msg": "user created", "user": schema.dump(user)}, 201


class UserSubscriptionResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      summary: Get a user subscription
      description: Get a single user subscription by ID
      parameters:
        - in: path
          name: user_subscription_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  user: UserSubscriptionSchema
        404:
          description: user does not exists
    put:
      tags:
        - api
      summary: Update a user
      description: Update a single user by ID
      parameters:
        - in: path
          name: user_subscription_id
          schema:
            type: integer
      requestBody:
        content:
          application/json:
            schema:
              UserSubscriptionSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: user updated
                  user: UserSubscriptionSchema
        404:
          description: user does not exists
    delete:
      tags:
        - api
      summary: Delete a user
      description: Delete a single user by ID
      parameters:
        - in: path
          name: user_subscription_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: user deleted
        404:
          description: user does not exists
    """

    method_decorators = [jwt_required()]

    def get(self, user_subscription_id):
        schema = UserSubscriptionSchema()
        user = UserSubsciprtion.query.get_or_404(user_subscription_id)
        return {"user_subscription": schema.dump(user)}

    def put(self, user_subscription_id):
        schema = UserSubscriptionSchema(partial=True)
        user = UserSubsciprtion.query.get_or_404(user_subscription_id)
        user = schema.load(request.json, instance=user)

        db.session.commit()

        return {"msg": "user subscription updated", "user": schema.dump(user)}

    def delete(self, user_subscription_id):
        user = UserSubsciprtion.query.get_or_404(user_subscription_id)
        db.session.delete(user)
        db.session.commit()

        return {"msg": "user subscription deleted"}


class UserSubscriptionList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      summary: Get a list of users
      description: Get a list of paginated users
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/UserSubscriptionSchema'
    post:
      tags:
        - api
      summary: Create a user subscription
      description: Create a new user subscription
      requestBody:
        content:
          application/json:
            schema:
              UserSubscriptionSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: user created
                  user: UserSubscriptionSchema
    """

    method_decorators = [jwt_required()]

    def get(self):
        schema = UserSubscriptionSchema(many=True)
        query = UserSubsciprtion.query
        return paginate(query, schema)

    def post(self):
        schema = UserSubscriptionSchema()
        user_subscription = schema.load(request.json)

        db.session.add(user_subscription)
        db.session.commit()

        return {"msg": "user subscription created", "user_subscription": schema.dump(user_subscription)}, 201
