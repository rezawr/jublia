from jublia.app import init_celery
from celery.schedules import crontab

app = init_celery()
app.conf.imports = app.conf.imports + ("jublia.tasks.mail_reminder",)

app.conf.beat_schedule = {
    "trigger-mail-queue-generating": {
        "task": "jublia.tasks.mail_reminder.generate_queue_email",
        "schedule": crontab()
    },
    "trigger-mail-queue-sending": {
        "task": "jublia.tasks.mail_reminder.sent_email",
        "schedule": crontab()
    }
}