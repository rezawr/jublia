from flask_mail import Message
from datetime import datetime
from jublia.app import init_mail
from jublia.extensions import db
from jublia.extensions import celery
from jublia.models import EventReminder, EventReminderMailQueue, UserSubsciprtion, MailStatusChoices

mail = init_mail()

@celery.task()
def generate_queue_email():
    try:
        todays_datetime = datetime(
            datetime.today().year,
            datetime.today().month,
            datetime.today().day,
            datetime.today().hour,
            datetime.today().minute
        )
        events = EventReminder.query.filter(EventReminder.timestamp==todays_datetime).all()
        users = UserSubsciprtion.query.all()

        for event in events:
            for user in users:
                e = EventReminderMailQueue(
                    event_reminder_id=event.id,
                    user_id=user.user.id,
                    status=MailStatusChoices.QUEUE
                )

                db.session.add(e)

        db.session.commit()
        sent_email.apply_async()
    except Exception as e:
        print(e)


@celery.task()
def sent_email():
    queues = EventReminderMailQueue.query.filter(EventReminderMailQueue.status==MailStatusChoices.QUEUE).all()
    group_recipients = {}
    group_mail = {}


    for queue in queues:
        queue.status = MailStatusChoices.PROCESS
        db.session.commit()

        if group_mail.get(queue.event_reminder_id) is None:
            group_mail[queue.event_reminder_id] = {}
            group_recipients[queue.event_reminder_id] = []

        group_mail[queue.event_reminder_id] = {
            'subject': queue.event_reminder.email_subject,
            'content': queue.event_reminder.email_content,
        }
        group_recipients[queue.event_reminder_id].append(queue.user.email)

    for key in group_mail.keys():
        try:
            msg = Message(group_mail[key]['subject'], sender = "", recipients = group_recipients[key])
            msg.body = group_mail[key]['content']
            mail.send(msg)

            queues = EventReminderMailQueue.query.filter(EventReminderMailQueue.status==MailStatusChoices.QUEUE).filter(EventReminderMailQueue.event_reminder_id==key).all()

            for queue in queues:
                queue.status = MailStatusChoices.SUCESS
                db.session.commit()
        except Exception as e:
            queues = EventReminderMailQueue.query.filter(EventReminderMailQueue.status==MailStatusChoices.QUEUE).filter(EventReminderMailQueue.event_reminder_id==key).all()

            for queue in queues:
                queue.status = MailStatusChoices.FAIL
                db.session.commit()

            print(e)

    print(group_recipients)
    print(group_mail)
