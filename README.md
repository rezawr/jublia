
# Assestment by Jublia

Reza Wahyu Ramadhan
[![Built with Cookiecutter Flask](https://img.shields.io/badge/built%20with-Cookiecutter%20Flask-ff69b4.svg?logo=cookiecutter)](https://github.com/karec/cookiecutter-flask-restful)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

## Installation

Install my project within your git. then try to install the requirement of the projects by using syntax below

```bash
  pip install -r requirements.txt
```

Right after finishing the Installation configurate the database in jublia/config.py. *note: I am not using environment variables because it cant be read while running the celery tasks, I dont have much time to figure it out why that happen.

Now you have to init your projects

```bash
  flask db init
  flask db migrate
  flask db upgrade
  flask init  # creates admin user
```

*note : default credential is username : admin, and password : admin
##  My Improvisation

After installation flask within cookiecutter projects template, the cookiecutter automatically create redoc-ui for api documentation, and Authentication flow with JWT. so i am using the cookiecutter flow template to finishing my assestment. The thing I've changed from the criteria that jublia told me is:

* Changed the endpoint */save_emails* to */api/v1/event/reminders*
* Build the event models so you have to create the models first **if i have any time, i will create the initiate data for the application*
* Build User models and User Subscription Model, so before you create the event reminders, you have to create at least the user subscription for the system generating queue for sending the emails.
* Using Pagination on every endpoint
* Created api documenatation that can be accessed on */redoc-ui*


