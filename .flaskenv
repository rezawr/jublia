FLASK_ENV=development
FLASK_APP=jublia.app:create_app
SECRET_KEY=mysecretkey
DATABASE_URI=postgresql://myuser:myuser@127.0.0.1:5432/jublia
CELERY_BROKER_URL=amqp://guest:guest@localhost/
CELERY_RESULT_BACKEND_URL=rpc://
